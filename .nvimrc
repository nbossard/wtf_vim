" Nicolas Bossard, august 2019
" VIM specific config file for Slides about vim (devday 2019)

" Activate spelling check for vue files and js files
set spelllang=en,fr
set spellfile=./vocabulary.utf-8.add
augroup spellfiletypes
    autocmd!
    autocmd FileType markdown,vue,javascript setlocal spell
augroup END

" Make slack work
" See doc at https://github.com/yaasita/edit-slack.vim

" Allow folding, based on syntax
set foldmethod=syntax "syntax highlighting items specify folds
set foldcolumn=1 "defines 1 col at window left, to indicate folding
let javaScript_fold=1 "activate folding by JS syntax
set foldlevelstart=99 "start file with all folds opened

" indentation is two spaces
set autoindent
set expandtab
set softtabstop=2
set shiftwidth=2


" Adding marker for max line length (120 )
augroup maxlinelingthmarker
  autocmd!
  autocmd FileType markdown,vue,javascript,plantuml setlocal colorcolumn=120
augroup END

" Launch NERDTree at start
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Use a custom technical dictionary for coc completion
set dictionary+=./coc-dico.txt

" Launch remark build on markdown save
autocmd BufWritePost pres.md echo "Generating remark slides!"
autocmd BufWritePost pres.md RemarkBuild

